## Description
Simple PoC - client application and node server to test websocket connection

To run the server:
`npm install` then 
`npm start`

Server is listening on port: `8000`
