const http = require('http');
const websocket = require('ws');

const server = http.createServer((req, res) => {
  res.end("I am connected");
});
const wss = new websocket.Server({ server });

wss.on('connection', (ws, req) => {

  ws.send(JSON.stringify({
    id: "test_id",
    type: "NEW_MESSAGE",
    data: "Server connection is established"
  }));

  ws.on('message', (message) => {
    const parsedMessage = JSON.parse(message);
    ws.send(JSON.stringify({
      id: "test_id",
      type: "NEW_MESSAGE",
      data: "Received " + parsedMessage.data
    }))
  });
});


server.listen(8000);
